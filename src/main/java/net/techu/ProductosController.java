package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductosController {
    
    private ArrayList<String> listaProductos = null;
    public ProductosController() {
        listaProductos = new ArrayList<>();
        listaProductos.add("PR1");
        listaProductos.add("PR2");
    }

    /*Set lista de productos*/
    //@RequestMapping(value ="/productos", method = RequestMethod.GET, produces = "application/json")
    @GetMapping(value ="/productos", produces = "application/json")
    public ResponseEntity<List<String>> obtenerlistado(){
        System.out.println("Estoy en obtener");
        return ResponseEntity.ok(listaProductos);
    }

    /*@RequestMapping(value = "productos/{id}", method = RequestMethod.GET)
        public String obtenerProductoPorId(@PathVariable("id") int id){
            String resultado = null;
            try {
                resultado = listaProductos.get(id);
            } catch (Exception ex){
              resultado = "No se ha encontrado el producto";
            }
            return resultado;
        }*/

    //@RequestMapping(value = "productos/{id}", method = RequestMethod.GET)
    @GetMapping("/productos/{id}")
    public ResponseEntity<String> obtenerProductoPorId(@PathVariable("id") int id) {
        String resultado = null;
        ResponseEntity<String> respuesta = null;
        try {
            resultado = listaProductos.get(id);
            //respuesta = new ResponseEntity(resultado, HttpStatus.OK);
            respuesta = ResponseEntity.ok(resultado);
        } catch (Exception ex) {
            resultado = "No se ha encontrado el producto";
            //respuesta = new ResponseEntity(resultado, HttpStatus.NOT_FOUND);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }


    /*Add nuevo producto*/
    //@RequestMapping(value = "/productos", method = RequestMethod.POST, produces = "applicarion/json")
    @PostMapping("/productos")
    public ResponseEntity<String> addProducto(){
        System.out.println("Estoy en añadir");
        listaProductos.add("NUEVO");
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    //@RequestMapping(value = "/productos/{nom}/{cat}", method = RequestMethod.POST, produces = "applicarion/json")
    @PostMapping(value = "/productos/{nom}/{cat}", produces = "applicarion/json")
    public ResponseEntity addProductoConNombre(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria){
        System.out.println("Voy a añadir el producto con nombre " + nombre + " y categoría " + categoria);
        listaProductos.add(nombre);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable int id){
        try{
            listaProductos.remove(id);
            return new ResponseEntity<>("Eliminado correctamente", HttpStatus.NO_CONTENT);
        }catch (Exception ex){
            return new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable int id){
        try{
            String productoAModificar = listaProductos.get(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch (Exception ex){
            return new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
    }

}